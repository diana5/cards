package com.example.cards.mapper;

import com.example.cards.dto.CardDTO;
import com.example.cards.entity.Card;
import com.example.cards.repository.CardRepository;
import com.example.cards.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ToCardEntityMapper {
    private Card cardEntity = new Card();
    private final UserRepository userRepository;

    private final CardRepository cardRepository;

    public ToCardEntityMapper(UserRepository userRepository, CardRepository cardRepository) {
        this.userRepository = userRepository;
        this.cardRepository = cardRepository;
    }


    public Card toCardEntity(CardDTO card) throws IOException {
        cardEntity.setDescription(card.getDescription());
        cardEntity.setId(card.getId());
        cardEntity.setName(card.getName());
        cardEntity.setStatus(card.getStatus());
        cardEntity.setColor(card.getColor());
        cardEntity.setUser(userRepository.findById(card.getUser_id()).get());
        return cardEntity;
    }


}
