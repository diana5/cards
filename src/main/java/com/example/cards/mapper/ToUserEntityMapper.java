package com.example.cards.mapper;

import com.example.cards.dto.UserDTO;
import com.example.cards.entity.User;

import com.example.cards.entity.Role;

import java.util.Optional;

public class ToUserEntityMapper {

    private final ToUserEntityMapper toUserEntityMapper;
    public ToUserEntityMapper(ToUserEntityMapper toUserEntityMapper) {
        this.toUserEntityMapper = toUserEntityMapper;
    }

    User user = new User();
    public User toUserEntity(Optional<UserDTO> userDTO) {
        user.setEmail(userDTO.get().getEmail());
        user.setId(userDTO.get().getId());
        user.setName(userDTO.get().getName());
        user.setPassword(userDTO.get().getPassword());
        return user;

    }
}
