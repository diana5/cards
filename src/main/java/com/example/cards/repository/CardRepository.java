package com.example.cards.repository;

import com.example.cards.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

    public Card save(Card cardEntity);

    public List<Card> findAll();

    public void deleteById(Long id);

    public List<Card> findByNameOrColor(String name,String color);

    public Optional<Card> findById(Long id);


}
