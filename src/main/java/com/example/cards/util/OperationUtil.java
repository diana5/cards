package com.example.cards.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OperationUtil {
    public static boolean isValidHexaCode(String str)
    {
        // Regex to check valid hexadecimal color code.
        String regex = "[a-zA-Z0-9]{6,}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the string is empty
        // return false
        if (str == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given string
        // and regular expression.
        Matcher m = p.matcher(str.substring(1));
        if((m.matches()) && (String.valueOf(str.charAt(0)).equals("#"))){

        // Return if the string
        // matched the ReGex && #
        return true; }
        else
            return false;
    }
}
