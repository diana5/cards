package com.example.cards.controller;

import com.example.cards.dto.CardDTO;
import com.example.cards.entity.Card;
import com.example.cards.mapper.ToCardEntityMapper;
import com.example.cards.repository.CardRepository;
import com.example.cards.services.CardService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/card")
public class CardController {

    private final ToCardEntityMapper ToCardEntiyMapper;
    private final CardService cardService;

    private final CardRepository cardRepository;

    public CardController(ToCardEntityMapper toCardEntiyMapper, CardService cardService, CardRepository cardRepository) {
        ToCardEntiyMapper = toCardEntiyMapper;
        this.cardService = cardService;
        this.cardRepository = cardRepository;
    }

    @PostMapping(value = "/create")
    private ResponseEntity createCard(@RequestBody CardDTO card) throws IOException {
    cardService.createCard(ToCardEntiyMapper.toCardEntity(card));
    return  ResponseEntity.ok("Card Created");
    }

    @GetMapping(value = "/getAll")
    private ResponseEntity<List<Card>> getCards(CardDTO card) {
        List<Card> response = cardService.findAll();
        return  ResponseEntity.ok((response));
    }

    @GetMapping(value = "/search/{name}/{color}")
    public List<Card> searchCard(@PathVariable(value = "name", required = false) String name,@PathVariable(value = "color", required = false) String color) {
        return cardRepository.findByNameOrColor(name,color);
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity deleteCard(Long idCard) {
        cardService.deleteCard(idCard);
        return ResponseEntity.ok("");
    }

    @GetMapping(value = "/get")
    private ResponseEntity get(Long idCard) {
        cardService.findCard(idCard);
        return ResponseEntity.ok("");
    }

    @PutMapping(value = "/update")
    private ResponseEntity update(Long idCard) {
        cardService.findCard(idCard);
        return ResponseEntity.ok("");
    }



}
