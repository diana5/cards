package com.example.cards.dto;

public class CardDTO {
    private Long id;
    private String description;
    private String name;
    private String color;
    private String status;

    private Long user_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public CardDTO(Long id, String description, String name, String color, String status, Long user_id) {
        this.id = id;
        this.description = description;
        this.name = name;
        this.color = color;
        this.status = status;
        this.user_id = user_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CardDTO() {}

}
