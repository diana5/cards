package com.example.cards.dto;


public class UserDTO {

    private Long id;
    private String name;
    private String email;
    private String password;

    private Long user_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDTO(Long id, String name, String email, String password, Long user_id) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.user_id = user_id;
    }


    public UserDTO() {};
}
