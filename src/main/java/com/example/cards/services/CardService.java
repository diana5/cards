package com.example.cards.services;

import com.example.cards.entity.Card;
import com.example.cards.repository.CardRepository;
import com.example.cards.util.OperationUtil;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CardService {
    public final CardRepository cardRepository;

    public CardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void createCard(Card card) throws IOException {

        /*Must change for a ENUM/*/
            if((OperationUtil.isValidHexaCode(card.getColor())) ){
                try {
                    card.setStatus("TO DO");
                    cardRepository.save(card);
                }
                catch (Exception e) {
                    throw new IOException("Invalid hexadecimal color");

                }

        } else  throw new IOException("Invalid hexadecimal color");

    }

    public List<Card> findAll(){
        return cardRepository.findAll();
    }
    public void deleteCard(Long id){
        cardRepository.deleteById(id);
    }

    public Optional<Card> findCard(Long id){
        return cardRepository.findById(id);
    }

    public void updateCard(Card card){
        Optional<Card> cardToUpdate = cardRepository.findById(card.getId());

        if(card!=null) {
            cardToUpdate.get().setStatus(card.getStatus());
            cardToUpdate.get().setColor(card.getColor());
            cardToUpdate.get().setDescription(card.getDescription());
            cardToUpdate.get().setName(card.getName());
        }
        cardRepository.save(cardToUpdate.get());

    }
}
